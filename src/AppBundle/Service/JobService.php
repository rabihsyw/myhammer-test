<?php
/**
 * Created by PhpStorm.
 * User: rabih
 * Date: 10/5/18
 * Time: 10:53 PM
 */

namespace AppBundle\Service;


use AppBundle\Entity\Job;
use AppBundle\Exceptions\ValidationException;
use AppBundle\Repository\CategoryRepository;
use AppBundle\Repository\CityRepository;
use AppBundle\Repository\JobRepository;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class JobService
{
    /**
     * @var JobRepository
     */
    private $repository;

    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var CityRepository
     */
    private $cityRepository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * JobService constructor.
     * @param JobRepository $repository
     */
    public function __construct(JobRepository $repository, CityRepository $cityRepository, CategoryRepository $categoryRepository, ValidatorInterface $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->cityRepository = $cityRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param Job $job
     * @throws ValidationException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function create(Job $job)
    {
        $job->setCity($this->cityRepository->findById($job->getCity()->getZipCode()));
        $job->setCategory($this->categoryRepository->findById($job->getCategory()->getId()));

        $errors = $this->validator->validate($job);
        if(count($errors) > 0) {
            $messages = [];
            /** @var \Symfony\Component\Validator\ConstraintViolation $error */
            foreach ($errors as $k => $error) {
                $messages[] = $error->getMessage();
            }
            throw new ValidationException($messages);
        }

        $this->repository->save($job);
    }
}