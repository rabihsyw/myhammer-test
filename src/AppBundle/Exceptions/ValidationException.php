<?php
/**
 * Created by PhpStorm.
 * User: rabih
 * Date: 10/6/18
 * Time: 3:45 PM
 */

namespace AppBundle\Exceptions;


class ValidationException extends \Exception
{

    /**
     * ValidationException constructor.
     */
    public function __construct(array $message = [], $code = 0)
    {
        parent::__construct(json_encode($message), $code);
    }
}