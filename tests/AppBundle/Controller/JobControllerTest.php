<?php
/**
 * Created by PhpStorm.
 * User: rabih
 * Date: 10/7/18
 * Time: 2:50 PM
 */

namespace App\Tests\AppBundle\Controller;


use App\Tests\AppBundle\BaseTestCase;

class JobControllerTest extends BaseTestCase
{
    public function testCreateJobShouldReturn201Status() {
        static::$client->request(
            'POST',
            '/job',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{ "title": "LEdjddd-C", "description": "Cotton cny, S, green", "done_date":"2018-10-10", "city": { "zip_code": "10115", "city": "Berlin" }, "category" : { "id":"108140", "name":"Kellersaniedrung" } }'
        );

        $crawler = static::$client->getResponse();

        $this->assertEquals(201, $crawler->getStatusCode());
    }

    public function testCreateJobShortTitleShouldReturn400Status() {
        static::$client->request(
            'POST',
            '/job',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{ "title": "LEd", "description": "Cotton cny, S, green", "done_date":"2018-10-10", "city": { "zip_code": "10115", "city": "Berlin" }, "category" : { "id":"108140", "name":"Kellersaniedrung" } }'
        );

        $crawler = static::$client->getResponse();

        $this->assertEquals(400, $crawler->getStatusCode());
    }

    public function testCreateJobUnValidCityShouldReturn400Status() {
        static::$client->request(
            'POST',
            '/job',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '{ "title": "Test5", "description": "Cotton cny, S, green", "done_date":"2018-10-10", "city": { "zip_code": "10115000", "city": "Berlin" }, "category" : { "id":"108140", "name":"Kellersaniedrung" } }'
        );

        $crawler = static::$client->getResponse();

        $this->assertEquals(400, $crawler->getStatusCode());
    }
}