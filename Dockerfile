FROM php:7.1-apache

WORKDIR /var/www/html

RUN apt-get update && apt-get install -y gnupg

RUN apt-key adv --keyserver pgp.mit.edu --recv-keys 5072E1F5 \
    && echo "deb http://repo.mysql.com/apt/debian/ jessie mysql-5.7" > /etc/apt/sources.list.d/mysql.list \
    && apt-get update \
    && echo "mysql-community-server mysql-community-server/root-pass password root" | debconf-set-selections \
    && echo "mysql-community-server mysql-community-server/re-root-pass password root" | debconf-set-selections \
    && apt-get install -y zlib1g-dev zip libzip4 unzip mysql-server \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install zip \
    && a2enmod rewrite headers \
    && sed -i "s/bind-address/#bind-address/" /etc/mysql/mysql.conf.d/mysqld.cnf

EXPOSE 3306

ENTRYPOINT [ "docker/entrypoint.sh" ]
