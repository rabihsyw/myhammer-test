<?php
/**
 * Created by PhpStorm.
 * User: rabih
 * Date: 10/5/18
 * Time: 7:50 PM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Job;
use AppBundle\Exceptions\ValidationException;
use AppBundle\Service\JobService;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

class JobController extends Controller
{
    /**
     * @var JobService
     */
    private $service;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * JobController constructor.
     * @param JobService $service
     */
    public function __construct(JobService $service)
    {
        $this->service = $service;
        $this->serializer = \JMS\Serializer\SerializerBuilder::create()->build();
    }


    /**
     * @Route("/job", methods={"POST"})
     * @SWG\Post(
     *     path="/job",
     *     summary="Create new job",
     *     operationId="create-job",
     *     tags={"job"},
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *     name="body",
     *          in="body",
     *          description="Job json object",
     *          required=true,
     *          @SWG\Schema(
     *              @SWG\Property(property="title", type="string"),
     *              @SWG\Property(property="description", type="string"),
     *              @SWG\Property(property="done_date", type="date", example="2017-01-01"),
     *              @SWG\Property(property="city", type="object",
     *                  @SWG\Property(property="zip_code", type="string"),
     *              ),
     *              @SWG\Property(property="category", type="object",
     *                  @SWG\Property(property="id", type="int"),
     *              ),
     *          ),
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Job added successfully!",
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="[]"
     *     ),
     * )
     */
    public function create(Request $request)
    {
        try {
            $this->service->create($this->serializer->deserialize($request->getContent(), Job::class, "json"));

            return new Response("Job added successfully!", 201);
        } catch(ValidationException $e) {

            return new Response($e->getMessage(), 400);
        } catch(\Exception $e) {

            return new Response(json_encode([$e->getMessage()]), 400);
        }
    }
}