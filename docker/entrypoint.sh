#!/usr/bin/env bash

service mysql start

sed "s/MYSQL_DATABASE/my_hammer/" docker/mysql/init.sql > /tmp/init.sql
sed -i "s/MYSQL_USER/root/" /tmp/init.sql
sed -i "s/MYSQL_PASSWORD/root/" /tmp/init.sql

mysql -proot < /tmp/init.sql

ln -s /var/run/mysqld/mysqld.sock /tmp/mysql.sock

#composer install
apache2-foreground