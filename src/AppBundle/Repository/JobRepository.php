<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Category;
use AppBundle\Entity\City;
use AppBundle\Entity\Job;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class JobRepository
 * @package AppBundle\Repository
 */
class JobRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Job $job
     * @throws \Doctrine\ORM\ORMException
     */
    public function save(Job $job)
    {
        $city = $this->entityManager->getReference(City::class, $job->getCity()->getZipCode());
        $cat = $this->entityManager->getReference(Category::class, $job->getCategory()->getId());
        $this->entityManager->persist($job->setCity($city)->setCategory($cat));
        $this->entityManager->flush();
    }
}
