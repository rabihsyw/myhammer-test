#!/usr/bin/env bash

docker exec -it myhammer_apache composer install
docker exec -it myhammer_apache composer setup
