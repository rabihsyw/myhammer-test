<?php

namespace App\Tests\AppBundle\Service;


use App\Tests\AppBundle\BaseTestCase;
use AppBundle\Entity\Category;
use AppBundle\Entity\City;
use AppBundle\Entity\Job;
use AppBundle\Exceptions\ValidationException;
use AppBundle\Service\JobService;

class JobServiceTest extends BaseTestCase
{
    /**
     * @var JobService
     */
    private $jobService;

    public function setUp()
    {
        $this->jobService = static::$container->get("AppBundle\Service\JobService");
    }

    public function testCreateJobShouldAddJobToDatabase() {
        $city = (new City())->setCity("Berlin")->setZipCode('10115');
        $cat = (new Category())->setId('108140')->setName('Kellersanierung');
        $newJob = (new Job())->setTitle("Test new Job")
            ->setDescription("Test description")
            ->setDoneDate(new \DateTime("2018-10-10"))
            ->setCity($city)
            ->setCategory($cat);
        $this->jobService->create($newJob);

        $res = static::$entityManager->createQueryBuilder()->select(["j"])
            ->from("AppBundle\Entity\Job", "j")
            ->where("j.title = :title")
            ->setParameter("title", $newJob->getTitle())
            ->getQuery()->getOneOrNullResult();

        $this->assertNotNull($res);
        $this->assertInstanceOf(Job::class, $res);
    }

    public function testCreateJobWithUnValidCityShouldThrowValidationException() {
        $this->expectException(ValidationException::class);

        $city = (new City())->setCity("Berlin")->setZipCode('1011500');
        $cat = (new Category())->setId('108140')->setName('Kellersanierung');
        $newJob = (new Job())->setTitle("Test Job")
            ->setDescription("Test description")
            ->setDoneDate(new \DateTime("2018-10-10"))
            ->setCity($city)
            ->setCategory($cat);
        $this->jobService->create($newJob);
    }

    public function testCreateJobWithUnValidCategoryShouldThrowValidationException() {
        $this->expectException(ValidationException::class);

        $city = (new City())->setCity("Berlin")->setZipCode('10115');
        $cat = (new Category())->setId('10814000')->setName('Kellersanierung');
        $newJob = (new Job())->setTitle("Test Job")
            ->setDescription("Test description")
            ->setDoneDate(new \DateTime("2018-10-10"))
            ->setCity($city)
            ->setCategory($cat);
        $this->jobService->create($newJob);
    }

    public function testCreateJobWithLongTitleShouldThrowValidationException() {
        $this->expectException(ValidationException::class);

        $city = (new City())->setCity("Berlin")->setZipCode('10115');
        $cat = (new Category())->setId('108140')->setName('Kellersanierung');
        $newJob = (new Job())->setTitle("Test Job 000000000000000000000000000000000000000000000")
            ->setDescription("Test description")
            ->setDoneDate(new \DateTime("2018-10-10"))
            ->setCity($city)
            ->setCategory($cat);
        $this->jobService->create($newJob);
    }

    public function testCreateJobWithShortTitleShouldThrowValidationException() {
        $this->expectException(ValidationException::class);

        $city = (new City())->setCity("Berlin")->setZipCode('10115');
        $cat = (new Category())->setId('108140')->setName('Kellersanierung');
        $newJob = (new Job())->setTitle("Test")
            ->setDescription("Test description")
            ->setDoneDate(new \DateTime("2018-10-10"))
            ->setCity($city)
            ->setCategory($cat);
        $this->jobService->create($newJob);
    }

    public function testCreateJobMissingTitleShouldThrowValidationException() {
        $this->expectException(ValidationException::class);

        $city = (new City())->setCity("Berlin")->setZipCode('10115');
        $cat = (new Category())->setId('108140')->setName('Kellersanierung');
        $newJob = (new Job())
            ->setDescription("Test description")
            ->setDoneDate(new \DateTime("2018-10-10"))
            ->setCity($city)
            ->setCategory($cat);
        $this->jobService->create($newJob);
    }
}