<?php

namespace AppBundle\DataFixtures;


use AppBundle\Entity\Category;
use AppBundle\Entity\City;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $this->persistCategories($manager);
        $this->persistCities($manager);

        $manager->flush();
    }

    private function persistCategories(ObjectManager $manager)
    {
        $data = [
            804040 => "Sonstige Umzugsleistungen",
            802030 => "Abtransport, Entsorgung undEntrümpelung",
            411070 => "Fensterreinigung",
            402020 => "Holzdielen schleifen",
            108140 => "Kellersanierung",
        ];

        foreach ($data as $k => $v) {
            $obj = new Category();
            $obj->setName($v);
            $obj->setId($k);
            $manager->persist($obj);
        }
    }

    private function persistCities(ObjectManager $manager)
    {
        $data = [
            '10115' => "Berlin",
            '32457' => "Porta Westfalica",
            '01623' => "Lommatzsch",
            '21521' => "Hamburg",
            '06895' => "Bülzig",
            '01612' => "Diesbar-Seußlitz",
        ];

        foreach ($data as $k => $v) {
            $obj = new City();
            $obj->setCity($v);
            $obj->setZipCode($k);
            $manager->persist($obj);
        }
    }
}