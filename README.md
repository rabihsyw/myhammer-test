## My Hammer - Test

##### Candidate name : Rabih Abou Zaid , [LinkedIn](https://www.linkedin.com/in/rabihsyw)


##### Technologies :

* PHP 7
* Symfony 3.4
* PHPUnit
* Docker
* Docker-Compose


##### Requirements :

* Docker
* Docker-Compose


##### Project setup :

*Make sure you are in the project directory.*

1. First you need to bring up the development environment:

    ```
    docker-compose up
    ```
    
    Two ports are exposed : `8080` and `3306`
    

2. Install all composer dependencies and setup the database and the predefined cities & categories:

    ```
    ./init.sh
    ```
    
3. Then you can either send requests to the server or you can run tests via the following command:

    ```
    ./run-tests.sh
    ```
    
4. For Frontend developers who wants to use this API they can see its documentation by calling:

    ```
    http://localhost:8080/api/doc.json
    ```