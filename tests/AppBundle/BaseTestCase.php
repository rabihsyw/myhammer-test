<?php
/**
 * Created by PhpStorm.
 * User: rabih
 * Date: 10/7/18
 * Time: 3:09 AM
 */

namespace App\Tests\AppBundle;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Process\Process;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BaseTestCase extends WebTestCase
{
    /**
     * @var Client
     */
    protected static $client;

    /**
     * @var ContainerInterface
     */
    protected static $container;

    /**
     * @var EntityManagerInterface
     */
    protected static $entityManager;

    public static function setUpBeforeClass()
    {
        static::$client = static::createClient();
        static::$container =  static::$client->getContainer();
        static::$entityManager =  static::$container->get('doctrine')
            ->getManager();

        $arr = [
            'bin/console doctrine:database:drop --if-exists --force',
            'bin/console doctrine:database:create',
            'bin/console doctrine:schema:update --force',
            'bin/console doctrine:fixtures:load --append'
        ];

        foreach ($arr as $item) {

            $p = new Process($item);
            $p->run();

            $content = $p->getOutput();

            print_r($content);
        }
    }
}